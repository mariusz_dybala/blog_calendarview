﻿using CalendarView.PageModels;
using FreshMvvm;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]

namespace CalendarView
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            RegisterDependencies();

            var mainPage = FreshPageModelResolver.ResolvePageModel<MainTabPageModel>();

            MainPage = mainPage;
        }

        private void RegisterDependencies()
        {
            // FreshIOC.Container.Register<ICalendarDaysGeneratorService, CalendarDaysProvider>();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}