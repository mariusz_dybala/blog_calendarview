using Xamarin.Forms;

namespace CalendarView.Controls.Calendar
{
    public partial class CalendarHeaderDayItem : StackLayout
    {
        public string HeaderDayText
        {
            set => HeaderDayLabel.Text = value;
        }
        
        public CalendarHeaderDayItem()
        {
            InitializeComponent();
        }
    }
}