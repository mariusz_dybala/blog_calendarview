using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using CalendarView.Enums;
using CalendarView.Models;
using CalendarView.Services;
using Xamarin.Forms;

namespace CalendarView.Controls.Calendar
{
    public partial class CalendarView : ContentView
    {
        private const int WeeksCount = 6;
        private static readonly int DaysInWeekCount = 7;

        private CalendarMonth _currentMonth;
        private int _dayMonthCount;
        private CalendarService _calendarService;

        private static readonly RowDefinition DefaultRowDefinition = new RowDefinition
        {
            Height = new GridLength(1, GridUnitType.Star)
        };

        private static readonly ColumnDefinition DefaultColumnDefinition = new ColumnDefinition
        {
            Width = new GridLength(1, GridUnitType.Star)
        };

        private CalendarSelectionMode _calendarSelectionMode;

        public CalendarSelectionMode CalendarSelectionMode
        {
            get => _calendarSelectionMode;
            set
            {
                _calendarSelectionMode = value;
                
                var selectedDays = _calendarService.GetAllSingleSelectedDays().Select(x=>x.DateTime).ToList();

                Initialize();
            
                if (!selectedDays.Any()) 
                    return;

                _calendarService.InitializeCalendarWithSelected(selectedDays);
                LoadNewMonth(selectedDays.FirstOrDefault());

            }
        }

        public static BindableProperty SelectedDatesProperty = BindableProperty.Create(nameof(SelectedDates),
            typeof(List<DateTime>),
            typeof(CalendarView), defaultBindingMode: BindingMode.TwoWay, propertyChanged: OnDatesChanged);


        public List<DateTime> SelectedDates
        {
            get => (List<DateTime>) GetValue(SelectedDatesProperty);
            set => SetValue(SelectedDatesProperty, value);
        }

        public static BindableProperty SubmitCommandProperty = BindableProperty.Create(nameof(SubmitCommand),
            typeof(ICommand),
            typeof(CalendarView));

        public ICommand SubmitCommand
        {
            get => (ICommand) GetValue(SubmitCommandProperty);
            set => SetValue(SubmitCommandProperty, value);
        }

        public static BindableProperty CancelCommandProperty = BindableProperty.Create(nameof(CancelCommand),
            typeof(ICommand),
            typeof(CalendarView));

        public ICommand CancelCommand
        {
            get => (ICommand) GetValue(CancelCommandProperty);
            set => SetValue(CancelCommandProperty, value);
        }

        public CalendarView()
        {
            InitializeComponent();
            GenerateCalendarGrid();
            AddDaysPlaceholdersToCalendar();
            Initialize();
        }

        private void Initialize()
        {
            _calendarService = new CalendarService(new CalendarDaysProvider(), CalendarSelectionMode);
            LoadNewMonth(DateTime.Now);
        }

        private void LoadNewMonth(DateTime dateTime)
        {
            _currentMonth = _calendarService.GetOrCreateMonthForDay(dateTime);
            _dayMonthCount = _currentMonth.Days.Count;

            FeedCalendarWithDays();
            UpdateTitle();
        }

        private void AddDaysPlaceholdersToCalendar()
        {
            var daysOfWeek = Utils.Extensions.GetMondayFirstWeekCollection();

            foreach (var dayInWeek in Enumerable.Range(1, DaysInWeekCount))
            {
                var dayLabel = new CalendarHeaderDayItem
                {
                    HeaderDayText = daysOfWeek[dayInWeek - 1].ToString().Substring(0, 3),
                };

                Calendar.Children.Add(dayLabel, dayInWeek - 1, 0);
            }

            foreach (var week in Enumerable.Range(1, WeeksCount))
            {
                foreach (var dayInWeek in Enumerable.Range(1, DaysInWeekCount))
                {
                    var calendarDayItem = new CalendarDayCardView();

                    Calendar.Children.Add(calendarDayItem, dayInWeek - 1, week);
                }
            }
        }

        private void FeedCalendarWithDays()
        {
            var iteration = 0;

            foreach (var week in Enumerable.Range(1, WeeksCount))
            {
                foreach (var dayInWeek in Enumerable.Range(1, DaysInWeekCount))
                {
                    var itemPosition = iteration + 7;
                    var dayControlInCalendarView = Calendar.Children[itemPosition];

                    if (dayControlInCalendarView is CalendarDayCardView calendarDayItem)
                    {
                        if (iteration > _dayMonthCount - 1)
                        {
                            calendarDayItem.ClearContext();
                            iteration++;
                            continue;
                        }

                        var dayInCalendar = _currentMonth.Days[iteration];
                        calendarDayItem.SetContext(dayInCalendar);

                        iteration++;
                    }
                }
            }
        }

        private void GenerateCalendarGrid()
        {
            Calendar.ColumnSpacing = 0;
            Calendar.RowSpacing = 0;

            //Add Row with days for Grid

            Calendar.RowDefinitions.Add(DefaultRowDefinition);

            // Add Rows for weeks

            foreach (var week in Enumerable.Range(1, WeeksCount))
            {
                Calendar.RowDefinitions.Add(DefaultRowDefinition);
            }

            //Add Columns for days

            foreach (var week in Enumerable.Range(1, WeeksCount))
            {
                Calendar.ColumnDefinitions.Add(DefaultColumnDefinition);
            }
        }

        private void NextMonthClicked(object sender, EventArgs e)
        {
            LoadNewMonth(_currentMonth.FullDate.AddMonths(1));
        }

        private void PreviousMonthClicked(object sender, EventArgs e)
        {
            LoadNewMonth(_currentMonth.FullDate.AddMonths(-1));
        }

        private void UpdateTitle()
        {
            CalendarTitle.Text = $"{_currentMonth.FullDate:MMMM} {_currentMonth.Year}";
        }

        private void OnCancelClicked(object sender, EventArgs e)
        {
            CancelCommand?.Execute(null);
        }

        private void OnSubmitClicked(object sender, EventArgs e)
        {
            var selectedDays = _calendarService.GetSelectedDaysForSelectionMode(CalendarSelectionMode);

            SelectedDates.Clear();
            SelectedDates.AddRange(selectedDays.Select(x => x.DateTime));

            SubmitCommand?.Execute(null);
        }

        private void OnClearClicked(object sender, EventArgs e)
        {
            _calendarService.ClearSelection();
        }

        private static void OnDatesChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            if (newvalue == null || oldvalue == newvalue)
                return;

            var selectedDates = newvalue as List<DateTime>;

            if (!selectedDates.Any())
                return;

            var calendarView = (CalendarView) bindable;
            calendarView._calendarService.InitializeCalendarWithSelected(selectedDates);
            calendarView.LoadNewMonth(selectedDates.FirstOrDefault());
        }

        private void OnSelectionModeChanged(object sender, ToggledEventArgs e)
        {
            CalendarSelectionMode = e.Value ? CalendarSelectionMode.Single : CalendarSelectionMode.Range;
        }
    }
}