using System;
using CalendarView.Models;
using Xamarin.Forms;

namespace CalendarView.Controls.Calendar
{
    public class CalendarDayCardView : ContentView
    {
        private Action _onSelected;

        private CalendarCardDay _context;

        private CalendarDayCardContentView _cardContentContent;

        public CalendarDayCardView()
        {
            Initialize();
            Padding = new Thickness(0);
        }
        
        private void Initialize()
        {
            _cardContentContent = new CalendarDayCardContentView(OnDaySelected);
            Content = _cardContentContent;
        }

        private void OnDaySelected()
        {
            _context?.OnCardSelected();
        }
        
        public void SetContext(CalendarCardDay calendarCardDay)
        {
            ClearContext();
            
            _context = calendarCardDay;

            _cardContentContent.InitializeDayWithContext(calendarCardDay);
            
            _context.UpdateDayCardSelection += UpdateSelectionFromContext;
        }

        private void UpdateSelectionFromContext()
        {
            _cardContentContent.UpdateViewSelection(_context);
        }       

        public void ClearContext()
        {
            if (_context == null)
            {
                return;
            }

            _context.UpdateDayCardSelection -= UpdateSelectionFromContext;
            _context = null;
            _cardContentContent.InitializeDayWithContext(null);
        }
    }
    
    public enum CalendarMode
    {
        SingleSelection,
        RangeSelection,
    }
}