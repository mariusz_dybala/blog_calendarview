using System;
using CalendarView.Enums;
using CalendarView.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Controls.Calendar
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CalendarDayCardContentView : Grid
    {
        public CalendarDayCardContentView(Action onItemClicked)
        {
            InitializeComponent();
            
            var gestureRecognizer = new TapGestureRecognizer();
            gestureRecognizer.Tapped += (sender, args) => onItemClicked();
            GestureRecognizers.Add(gestureRecognizer);
        }

        public void InitializeDayWithContext(CalendarCardDay calendarCardDay)
        {
            if (calendarCardDay == null)
            {
                Clear();
                return;
            }
            
            NumberLabel.Text = calendarCardDay.DateTime.Date.Day.ToString();
            UpdateViewSelection(calendarCardDay);
        }

        public void UpdateViewSelection(CalendarCardDay calendarCardDay)
        {
            if (calendarCardDay.SelectionType == SelectionType.None)
            {
                UnSelectDay(calendarCardDay);
                return;
            }

            SelectDay(calendarCardDay.SelectionType);
        }

        private void Clear()
        {
            NumberLabel.Text = string.Empty;
            CircleFrame.BackgroundColor = Color.White;
            NumberLabel.TextColor = Color.White;

            LeftBackgroundBoxView.BackgroundColor = Color.White;
            RightBackgroundBoxView.BackgroundColor = Color.White;
        }

        private void SelectDay(SelectionType selectionType)
        {
            CircleFrame.BackgroundColor = Color.Red;
            NumberLabel.TextColor = Color.White;

            LeftBackgroundBoxView.BackgroundColor = Color.White;
            RightBackgroundBoxView.BackgroundColor = Color.White;

            if (selectionType == SelectionType.Single)
            {
                return;
            }

            if (selectionType == SelectionType.FirstInRange)
            {
                RightBackgroundBoxView.BackgroundColor = Color.Red;
                return;
            }

            if (selectionType == SelectionType.LastInRange)
            {
                LeftBackgroundBoxView.BackgroundColor = Color.Red;
                return;
            }

            if (selectionType == SelectionType.InRange)
            {
                LeftBackgroundBoxView.BackgroundColor = Color.Red;
                RightBackgroundBoxView.BackgroundColor = Color.Red;
            }
        }

        private void UnSelectDay(CalendarCardDay calendarCardDay)
        {
            LeftBackgroundBoxView.BackgroundColor = Color.White;
            RightBackgroundBoxView.BackgroundColor = Color.White;

            if (!calendarCardDay.IsInSelectedMonth)
            {
                SetAsNotCurrentMonth();
                return;
            }

            if (calendarCardDay.IsWeekendDay)
            {
                SetAsWeekend();
                return;
            }

            SetAsCurrentMonth();
        }

        private void SetAsWeekend()
        {
            CircleFrame.BackgroundColor = Color.Transparent;
            NumberLabel.TextColor = Color.OrangeRed;
        }

        private void SetAsNotCurrentMonth()
        {
            CircleFrame.BackgroundColor = Color.Transparent;
            NumberLabel.TextColor = Color.DarkGray;
        }

        private void SetAsCurrentMonth()
        {
            CircleFrame.BackgroundColor = Color.Transparent;
            NumberLabel.TextColor = Color.SlateGray;
        }
    }
}