using System;
using Xamarin.Forms;

namespace CalendarView.Controls
{
    public class StackButton : StackLayout
    {
        public event Action<object> Clicked;
        
        public StackButton()
        {
            var gestureRecognizer = new TapGestureRecognizer();
            gestureRecognizer.Tapped += OnClick;
            GestureRecognizers.Add(gestureRecognizer);
        }

        private void OnClick(object sender, EventArgs e)
        {
            Clicked?.Invoke(sender);
        }
    }
}