using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabButton : StackButton
    {
        public int TabButtonIndex { get; set; }
        public FileImageSource TabImageSource
        {
            set => TabIcon.Source = value;
        }
        
        public TabButton()
        {
            InitializeComponent();
        }
    }
}