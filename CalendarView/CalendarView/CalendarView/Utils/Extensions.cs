using System;
using System.Collections.Generic;

namespace CalendarView.Utils
{
    public static class Extensions
    {
        public static IList<DayOfWeek> GetMondayFirstWeekCollection()
        {
            return new List<DayOfWeek>
            {
                DayOfWeek.Monday, 
                DayOfWeek.Tuesday, 
                DayOfWeek.Wednesday, 
                DayOfWeek.Thursday, 
                DayOfWeek.Friday,
                DayOfWeek.Saturday,
                DayOfWeek.Sunday
            };
        }

        public static DateTime FirstDayOfMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1);
        }
    }
}