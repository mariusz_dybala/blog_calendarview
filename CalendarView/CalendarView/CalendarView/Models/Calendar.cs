using System;
using System.Collections.Generic;
using System.Linq;

namespace CalendarView.Models
{
    public class Calendar
    {
        public IList<CalendarMonth> Months { get; } = new List<CalendarMonth>();

        public IEnumerable<CalendarCardDay> GetDaysFromCalendarForDate(DateTime dateTime)
        {
            var days = new List<CalendarCardDay>();
            var dayMonth = GetMonth(dateTime);
            var dayFromCurrentMonth = GetDayCardFromMonthIfExists(dayMonth, dateTime);
            
            var previousMonth = GetPrevious(dateTime);
            var previousMonthDay = GetDayCardFromMonthIfExists(previousMonth, dateTime);
            
            var nextMonth = GetNextMonth(dateTime);
            var nextMonthDay = GetDayCardFromMonthIfExists(nextMonth, dateTime);
            
            if(dayFromCurrentMonth != null)
                days.Add(dayFromCurrentMonth);
            if(previousMonthDay != null)
                days.Add(previousMonthDay);
            if(nextMonthDay != null)
                days.Add(nextMonthDay);

            return days;
        }

        public CalendarMonth GetMonth(DateTime dateTime)
        {
            var calendarMonth = Months.FirstOrDefault(x => x.Equals(dateTime));
            return calendarMonth;
        }

        public bool CheckIfMonthForDateExist(DateTime dateTime)
        {
            var monthForDate = GetMonth(dateTime);
            return monthForDate != null;
        }

        public IEnumerable<CalendarMonth> GetMonthsForRange(DateTime startMonthDate, DateTime endMonthDate)
        {
            return Months.Where(x => x >= startMonthDate && x <= endMonthDate);
        }
        
        public IEnumerable<CalendarMonth> GetMonthsWithDays(DateTime startMonthDate, DateTime endMonthDate)
        {
            var monthsInRange = Months.Where(x => x >= startMonthDate && x <= endMonthDate).ToList();

            var firstMonthInRange = monthsInRange.FirstOrDefault();

            if (firstMonthInRange != null)
            {
                var monthBefore = GetPrevious(firstMonthInRange.FullDate);
                
                if(monthBefore != null && monthBefore.Days.Any(x=>x.Equals(startMonthDate)))
                    monthsInRange.Insert(0,monthBefore);
            }

            var lastMonthInRange = monthsInRange.LastOrDefault();

            if (lastMonthInRange != null)
            {
                var monthAfter = GetNextMonth(lastMonthInRange.FullDate);
                if(monthAfter != null && monthAfter.Days.Any(x=>x.Equals(endMonthDate)))
                    monthsInRange.Add(monthAfter);
            }

            return monthsInRange;
        }

        public void AddMonthIfNotExist(CalendarMonth month)
        {
            if(!Months.Any())
                Months.Add(month);
            
            if(CheckIfMonthForDateExist(month.FullDate))
                return;
            
            var firstMonth = Months.FirstOrDefault();

            if(month.FullDate < firstMonth.FullDate)
                Months.Insert(0,month);
            else
                Months.Add(month);
        }

        private CalendarMonth GetPrevious(DateTime currentMonthDate)
        {
            var currentMonth = GetCurrentMonthForDate(currentMonthDate);

            if (currentMonth == null)
                return null;

            var indexCurrentMonth = Months.IndexOf(currentMonth);

            if (indexCurrentMonth == 0)
                return null;

            return Months[indexCurrentMonth - 1];
        }

        private CalendarMonth GetNextMonth(DateTime currentMonthDate)
        {
            var currentMonth = GetCurrentMonthForDate(currentMonthDate);

            if (currentMonth == null)
                return null;

            var indexCurrentMonth = Months.IndexOf(currentMonth);

            if (indexCurrentMonth == Months.Count - 1)
                return null;

            return Months[indexCurrentMonth + 1];
        }
        
        private CalendarCardDay GetDayCardFromMonthIfExists(CalendarMonth month, DateTime dateTime)
        {
            if (month == null)
                return null;
            
            var day = month.Days.FirstOrDefault(x => x.Equals(dateTime));

            return day;
        }

        private CalendarMonth GetCurrentMonthForDate(DateTime monthDate)
        {
            var currentMonth = Months.FirstOrDefault(x => x.Equals(monthDate));
            return currentMonth;
        }
    }
}