using System;
using System.Collections.Generic;

namespace CalendarView.Models
{
    public class CalendarMonth : IEquatable<DateTime>
    {
        public DateTime FullDate { get; }
        public int Year { get; set; }
        public int Month { get; set; }
        public int NumberOfWeeks { get; set; }
        
        public IList<CalendarCardDay> Days { get; set; }

        public CalendarMonth(DateTime dayOfMonth, IList<CalendarCardDay> days)
        {
            Year = dayOfMonth.Year;
            Month = dayOfMonth.Month;
            NumberOfWeeks = days.Count / 7;
            Days = days;
            FullDate = new DateTime(Year, Month, 1);
        }
        
        public bool Equals(DateTime other)
        {
            return Year == other.Year && Month == other.Month;
        }
                
        public static bool operator>= (CalendarMonth month, DateTime thatDate) 
        {
            if (month.Month == thatDate.Month && month.Year == thatDate.Year)
                return true;
            var startMonth = new DateTime(thatDate.Year, thatDate.Month, 1);

            return month.FullDate > startMonth;
        }
        
        public static bool operator<= (CalendarMonth month, DateTime thatDate) {
            if (month.Month == thatDate.Month && month.Year == thatDate.Year)
                return true;
            var startMonth = new DateTime(thatDate.Year, thatDate.Month, 1);

            return month.FullDate < startMonth;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Year;
                hashCode = (hashCode * 397) ^ Month;
                hashCode = (hashCode * 397) ^ NumberOfWeeks;
                hashCode = (hashCode * 397) ^ (Days != null ? Days.GetHashCode() : 0);
                return hashCode;
            }
        }
    }
}