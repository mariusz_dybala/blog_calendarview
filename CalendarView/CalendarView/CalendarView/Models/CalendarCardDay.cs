using System;
using CalendarView.Enums;
using CalendarView.Services;

namespace CalendarView.Models
{
    public class CalendarCardDay : IEquatable<CalendarCardDay>, IEquatable<DateTime>
    {
        private readonly CalendarService _calendarService;
        public DateTime DateTime { get; set; }
        public bool IsWeekendDay { get; set; }
        
        public bool IsInSelectedMonth { get; set; }
        
        public bool IsFirstDayInCalendarMonth { get; set; }
        public bool IsLastDayInCalendarMonth { get; set; }
        public SelectionType SelectionType { get; private set; }
        
        public bool IsSelected { get; set; }

        public CalendarCardDay(CalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        public event Action UpdateDayCardSelection; 
        
        public bool Equals(DateTime other)
        {
            return DateTime.Year == other.Year 
                   && DateTime.Month == other.Month 
                   && DateTime.Day == other.Day;
        }

        public bool Equals(CalendarCardDay other)
        {
            return other != null && Equals(other.DateTime);
        }

        public void OnCardSelected()
        {
            _calendarService.HandleDaySelected(this);
        }

        public void ApplySelection(SelectionType selectionType)
        {
            SelectionType = selectionType;

            IsSelected = true;
            
            UpdateDayCardSelection?.Invoke();
        }
        
        public void ApplyUnSelection()
        {
            IsSelected = false;
            
            if (SelectionType == SelectionType.None)
            {
                return;
            }
            SelectionType = SelectionType.None;
            
            UpdateDayCardSelection?.Invoke();
        }
    }
}