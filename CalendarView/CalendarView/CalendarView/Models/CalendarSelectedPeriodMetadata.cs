using System;

namespace CalendarView.Models
{
    public class CalendarSelectedPeriodMetadata
    {
        public DateTime SelectedMonth { get; set; }
        public DateTime SelectedMonthPrevious { get; set; }
        public DateTime SelectedMonthNext { get; set; }
        
        public int SelectedMonthDaysInMonthCount { get; set; }
        public int SelectedMonthPreviousDaysInMonthCount { get; set; }
        public int SelectedMonthNextDaysInMonthCount { get; set; }
        
        public int SelectedMonthFirstDayInWeekNumber { get; set; }
        public int SelectedMonthLastDayInWeekNumber { get; set; }
    }
}