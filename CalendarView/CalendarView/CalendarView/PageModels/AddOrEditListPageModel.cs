using System.Windows.Input;
using CalendarView.Models;
using FreshMvvm;
using PropertyChanged;
using CalendarView.Interfaces;
using Xamarin.Forms;

namespace CalendarView.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class AddOrEditListPageModel : FreshBasePageModel
    {
        private  IAddOrEditStrategy _addOrEditStrategy;
        private ShoppingList _shoppingListModel;
        private string _buttonText;
        private string _headerText;

        public ShoppingList ShoppingListModel
        {
            get => _shoppingListModel;
            set => _shoppingListModel = value;
        }

        public string ButtonText
        {
            get => _buttonText;
            set => _buttonText = value;
        }
        
        public string HeaderText
        {
            get => _headerText;
            set => _headerText = value;
        }

        public ICommand ButtonClickedCommand => new Command(OnButtonClicked);
        public ICommand CancelClickedCommand => new Command(OnCancelClicked);

        public override void Init(object message)
        {
            if (message is IAddOrEditStrategy addOrEditPageService)
            {
                _addOrEditStrategy = addOrEditPageService;
                ShoppingListModel = ShoppingList.Copy(addOrEditPageService.ShoppingListModel);
                ButtonText = addOrEditPageService.ButtonText;
                HeaderText = addOrEditPageService.HeaderText;
            }
        }

        private void OnButtonClicked()
        {
            _addOrEditStrategy.OnButtonClicked(ShoppingListModel, CoreMethods);
        }

        private void OnCancelClicked()
        {
            CoreMethods.PopPageModel(true, true);
        }
    }
}