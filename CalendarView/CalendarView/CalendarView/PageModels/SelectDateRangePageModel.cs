using System;
using System.Collections.Generic;
using System.Windows.Input;
using FreshMvvm;
using Xamarin.Forms;

namespace CalendarView.PageModels
{
    public class SelectDateRangePageModel : FreshBasePageModel
    {
        public List<DateTime> SelectedDates { get; set; }
        
        public ICommand CloseCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand SubmitCommand { get; set; }
        public SelectDateRangePageModel()
        {
            CloseCommand = new Command(OnClose);
            CancelCommand = new Command(OnClose);
            SubmitCommand = new Command(OnSubmit);

            SelectedDates = new List<DateTime>
            {
                DateTime.Now.AddDays(-10), 
                DateTime.Now.AddDays(-9), 
                DateTime.Now.AddDays(-8), 
                DateTime.Now.AddDays(-7), 
                DateTime.Now.AddDays(-6), 
                DateTime.Now.AddDays(-5), 
                DateTime.Now.AddDays(-4), 
                DateTime.Now.AddDays(-3), 
                DateTime.Now.AddDays(-2), 
                DateTime.Now.AddDays(-1), 
                DateTime.Now.AddDays(0), 
                DateTime.Now.AddDays(1), 
                DateTime.Now.AddDays(2), 
                DateTime.Now.AddDays(3), 
                DateTime.Now.AddDays(4), 
                DateTime.Now.AddDays(5), 
                DateTime.Now.AddDays(6), 
                DateTime.Now.AddDays(7), 
                DateTime.Now.AddDays(8), 
                DateTime.Now.AddDays(9), 
                DateTime.Now.AddDays(10) 
            };
        }

        private void OnSubmit()
        {
            CoreMethods.PopPageModel(true);
        }

        private void OnClose()
        {
            CoreMethods.PopPageModel(true);
        }
    }
}