using System;
using System.Collections.Generic;
using System.Linq;
using CalendarView.Interfaces.Calendar;
using CalendarView.Models;

namespace CalendarView.Services
{
    public class CalendarDaysProvider : ICalendarDaysProvider
    {
        private static int FistDayInWeekNumber = 1;
        private static int LastDayInWeekNumber = 7;

        private CalendarMonth _calendarMonth;

        public CalendarMonth GenerateCalendarMonth(DateTime selectedDay, CalendarService calendarService)
        {

            var meta = GenerateMetadataOnUserSelectedPeriod(selectedDay);
            var days = GenerateDaysForCalendarPage(meta, calendarService);
            _calendarMonth = new CalendarMonth(selectedDay, days);

            return _calendarMonth;
        }

        private CalendarSelectedPeriodMetadata GenerateMetadataOnUserSelectedPeriod(DateTime selectedPeriod)
        {
            var selectedDayMetadata = new CalendarSelectedPeriodMetadata();

            var selectedDate = new DateTime(selectedPeriod.Year, selectedPeriod.Month, 1);
            var selectedDateMinus1 = selectedDate.AddMonths(-1);
            var selectedDatePlus1 = selectedDate.AddMonths(1);

            var daysInMonthMinus1 = DateTime.DaysInMonth(selectedDateMinus1.Year, selectedDateMinus1.Month);
            var daysInMonth = DateTime.DaysInMonth(selectedDate.Year, selectedDate.Month);
            var daysInMonthPlus1 = DateTime.DaysInMonth(selectedDatePlus1.Year, selectedDatePlus1.Month);

            var firstDayOfSelectedMonth = new DateTime(selectedDate.Year, selectedDate.Month, 1);
            var lastDayOfSelectedMonth = new DateTime(selectedDate.Year, selectedDate.Month, daysInMonth);

            var dayOfWeekForFirstDay = firstDayOfSelectedMonth.DayOfWeek;
            var dayOfWeekForLastDay = lastDayOfSelectedMonth.DayOfWeek;

            var dayOfWeekForFirstDayNumber = MapDayOfWeekToNumber(dayOfWeekForFirstDay);
            var dayOfWeekForLastDayNumber = MapDayOfWeekToNumber(dayOfWeekForLastDay);

            selectedDayMetadata.SelectedMonth = selectedDate;
            selectedDayMetadata.SelectedMonthPrevious = selectedDateMinus1;
            selectedDayMetadata.SelectedMonthNext = selectedDatePlus1;

            selectedDayMetadata.SelectedMonthDaysInMonthCount = daysInMonth;
            selectedDayMetadata.SelectedMonthPreviousDaysInMonthCount = daysInMonthMinus1;
            selectedDayMetadata.SelectedMonthNextDaysInMonthCount = daysInMonthPlus1;

            selectedDayMetadata.SelectedMonthFirstDayInWeekNumber = dayOfWeekForFirstDayNumber;
            selectedDayMetadata.SelectedMonthLastDayInWeekNumber = dayOfWeekForLastDayNumber;

            return selectedDayMetadata;
        }

        private static IList<CalendarCardDay> GenerateDaysForCalendarPage(
            CalendarSelectedPeriodMetadata selectedDateMetadata, CalendarService calendarService)
        {
            var daysInPage = new List<CalendarCardDay>();

            var daysForSelectedMonth = Enumerable.Range(1, selectedDateMetadata.SelectedMonthDaysInMonthCount);

            foreach (var dayForSelectedMonth in daysForSelectedMonth)
            {
                var day = new CalendarCardDay(calendarService);

                day.DateTime = new DateTime(selectedDateMetadata.SelectedMonth.Year,
                    selectedDateMetadata.SelectedMonth.Month, dayForSelectedMonth);
                day.IsWeekendDay = IsWeekendDay(day.DateTime);
                day.IsInSelectedMonth = true;

                daysInPage.Add(day);
            }

            AddDaysFromPreviousMonthIfNeeded(daysInPage, selectedDateMetadata, calendarService);
            AddDaysFromNextMonthIfNeeded(daysInPage, selectedDateMetadata, calendarService);
            
            daysInPage.First().IsFirstDayInCalendarMonth = true;
            daysInPage.Last().IsLastDayInCalendarMonth = true;

            return daysInPage;
        }

        private static void AddDaysFromNextMonthIfNeeded(List<CalendarCardDay> daysInPage,
            CalendarSelectedPeriodMetadata selectedDateMetadata, CalendarService calendarService)
        {
            if (selectedDateMetadata.SelectedMonthLastDayInWeekNumber == LastDayInWeekNumber)
            {
                return;
            }

            var offset = LastDayInWeekNumber - selectedDateMetadata.SelectedMonthLastDayInWeekNumber;

            var daysFromNextMonth = Enumerable.Range(1, offset);

            var daysToAdd = new List<CalendarCardDay>();

            foreach (var dayFromNextMonth in daysFromNextMonth)
            {
                var day = new CalendarCardDay(calendarService);

                day.DateTime = new DateTime(selectedDateMetadata.SelectedMonthNext.Year,
                    selectedDateMetadata.SelectedMonthNext.Month, dayFromNextMonth);
                day.IsWeekendDay = IsWeekendDay(day.DateTime);

                daysToAdd.Add(day);
            }

            daysInPage.AddRange(daysToAdd);
        }

        private static void AddDaysFromPreviousMonthIfNeeded(List<CalendarCardDay> daysInPage,
            CalendarSelectedPeriodMetadata selectedDateMetadata, CalendarService calendarService)
        {
            if (selectedDateMetadata.SelectedMonthFirstDayInWeekNumber == FistDayInWeekNumber)
            {
                return;
            }

            var offset = selectedDateMetadata.SelectedMonthFirstDayInWeekNumber - 1;

            var daysFromPreviousMonth =
                Enumerable.Range(selectedDateMetadata.SelectedMonthPreviousDaysInMonthCount - (offset - 1), offset);

            var daysToAdd = new List<CalendarCardDay>();

            foreach (var dayFromNextMonth in daysFromPreviousMonth)
            {
                var day = new CalendarCardDay(calendarService);

                day.DateTime = new DateTime(selectedDateMetadata.SelectedMonthPrevious.Year,
                    selectedDateMetadata.SelectedMonthPrevious.Month, dayFromNextMonth);
                day.IsWeekendDay = IsWeekendDay(day.DateTime);

                daysToAdd.Add(day);
            }

            daysInPage.InsertRange(0, daysToAdd);
        }

        private static bool IsWeekendDay(DateTime day)
        {
            var dayOfWeek = day.DayOfWeek;
            if (dayOfWeek == DayOfWeek.Saturday || dayOfWeek == DayOfWeek.Sunday)
            {
                return true;
            }

            return false;
        }

        private static int MapDayOfWeekToNumber(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return LastDayInWeekNumber;
                case DayOfWeek.Monday:
                    return FistDayInWeekNumber;
                case DayOfWeek.Tuesday:
                    return FistDayInWeekNumber + 1;
                case DayOfWeek.Wednesday:
                    return FistDayInWeekNumber + 2;
                case DayOfWeek.Thursday:
                    return FistDayInWeekNumber + 3;
                case DayOfWeek.Friday:
                    return FistDayInWeekNumber + 4;
                case DayOfWeek.Saturday:
                    return FistDayInWeekNumber + 5;
                default:
                    throw new ArgumentOutOfRangeException(nameof(dayOfWeek), dayOfWeek, null);
            }
        }
    }
}