using CalendarView.Enums;
using CalendarView.Models;

namespace CalendarView.Services
{

    public interface IDaySelector
    {
        void HandleSelection(CalendarCardDay calendarCardDay);
        void Reset();
    }

    public class SingleDaySelector : IDaySelector
    {
        private readonly ICalendarService _calendarService;

        public SingleDaySelector(ICalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        public void HandleSelection(CalendarCardDay newSelectedDay)
        {
            if (newSelectedDay.SelectionType == SelectionType.Single)
            {
                _calendarService.ApplyUnSelectionForDay(newSelectedDay.DateTime);
                return;
            }
            
            _calendarService.ApplySelectionForDay(newSelectedDay.DateTime);
        }

        public void Reset()
        {
        }
    }
    
    
    public class DaySelector : IDaySelector
    {
        private readonly ICalendarService _calendarService;
        private CalendarCardDay _firstSelectedDay;
        private CalendarCardDay _lastSelectedDay;

        public DaySelector(ICalendarService calendarService)
        {
            _calendarService = calendarService;
        }

        public void Reset()
        {
            _firstSelectedDay = null;
            _lastSelectedDay = null;
        }

        public void HandleSelection(CalendarCardDay newSelectedDay)
        {
            if (_firstSelectedDay != null)
            {
                if (_firstSelectedDay.DateTime == newSelectedDay.DateTime)
                {
                    _lastSelectedDay = null;
                    _calendarService.ApplySelectionForDay(_firstSelectedDay.DateTime, true);
                }
                else if (_lastSelectedDay != null)
                {
                    //7
                    if (newSelectedDay.DateTime > _lastSelectedDay.DateTime)
                    {
                        _lastSelectedDay = newSelectedDay;
                        
                        _calendarService.ApplySelectionForDays(_firstSelectedDay.DateTime,
                            _lastSelectedDay.DateTime);
                    }
                    //6
                    else if (newSelectedDay.DateTime < _lastSelectedDay.DateTime)
                    {
                        _firstSelectedDay = newSelectedDay;
                        
                        _calendarService.ApplySelectionForDays(_firstSelectedDay.DateTime,
                            _lastSelectedDay.DateTime);
                    }
                    //8
                    else if (newSelectedDay.DateTime < _lastSelectedDay.DateTime && 
                             newSelectedDay.DateTime > _firstSelectedDay.DateTime)
                    {
                        _lastSelectedDay = newSelectedDay;
                        
                        _calendarService.ApplySelectionForDays(_firstSelectedDay.DateTime,
                            _lastSelectedDay.DateTime);
                    }
                    //4
                    else if (newSelectedDay.DateTime == _firstSelectedDay.DateTime)
                    {
                        _lastSelectedDay = null;
                        
                        _calendarService.ApplySelectionForDay(newSelectedDay.DateTime, true);
                    }
                    
                    //4
                    else if (newSelectedDay.DateTime == _lastSelectedDay.DateTime)
                    {
                        _firstSelectedDay = _lastSelectedDay;
                        _lastSelectedDay = null;
                        
                        _calendarService.ApplySelectionForDay(newSelectedDay.DateTime, true);
                    }
                                       
                    //5
                    else if (newSelectedDay.DateTime == _lastSelectedDay.DateTime)
                    {
                        _firstSelectedDay = _lastSelectedDay;
                        _lastSelectedDay = null;
                        
                        _calendarService.ApplySelectionForDay(newSelectedDay.DateTime, true);
                    }
                }
                else
                {
                    //9
                    if (newSelectedDay.DateTime < _firstSelectedDay.DateTime)
                    {
                        _lastSelectedDay = _firstSelectedDay;
                        _firstSelectedDay = newSelectedDay;
                        
                        _calendarService.ApplySelectionForDays(_firstSelectedDay.DateTime,
                            _lastSelectedDay.DateTime);
                    }
                    //3
                    else if (newSelectedDay.DateTime > _firstSelectedDay.DateTime)
                    {
                        _lastSelectedDay = newSelectedDay;
                        
                        _calendarService.ApplySelectionForDays(_firstSelectedDay.DateTime,
                            _lastSelectedDay.DateTime);
                    }
                    
                    //2
                    else if (newSelectedDay.DateTime == _firstSelectedDay.DateTime)
                    {
                        _firstSelectedDay = null;
                        _calendarService.ApplySelectionForDay(newSelectedDay.DateTime, true);
                    }
                }
            }
            else
            {
                //1
                if (_lastSelectedDay == null)
                {
                    _firstSelectedDay = newSelectedDay;
                    _calendarService.ApplySelectionForDay(newSelectedDay.DateTime, true);
                }
            }
        }
    }
}