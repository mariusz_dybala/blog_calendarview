using System;
using System.Collections.Generic;
using System.Linq;
using CalendarView.Enums;
using CalendarView.Interfaces.Calendar;
using CalendarView.Models;
using CalendarView.Utils;

namespace CalendarView.Services
{
    public interface ICalendarService
    {
        void ApplySelectionForDays(DateTime startDate, DateTime endDate);
        void ApplySelectionForDay(DateTime dayForSelection, bool withCleanUp = false);
        void ApplyUnSelectionForDay(DateTime dayForSelection);
    }

    public class CalendarService : ICalendarService
    {
        private readonly CalendarSelectionMode SelectionMode;
        private readonly Calendar _calendar;

        private readonly ICalendarDaysProvider _calendarDaysProvider;
        private IDaySelector _daySelector;

        public CalendarService(ICalendarDaysProvider calendarDaysProvider, CalendarSelectionMode selectionMode)
        {
            _calendar = new Calendar();
            _calendarDaysProvider = calendarDaysProvider;
            SelectionMode = selectionMode;

            InitializeDaySelector();
        }

        private void InitializeDaySelector()
        {
            if (SelectionMode == CalendarSelectionMode.Range)
            {
                _daySelector = new DaySelector(this);
            }
            else
            {
                _daySelector = new SingleDaySelector(this);
            }
        }

        public void InitializeCalendarWithSelected(List<DateTime> selectedDays)
        {
            if (!selectedDays.Any())
                return;

            var orderedDays = selectedDays.OrderBy(x => x);

            if (SelectionMode == CalendarSelectionMode.Single)
            {
                foreach (var day in orderedDays)
                {
                    CreateMonthForDay(day);
                    ApplySelectionForDay(day);
                }

                return;
            }

            var firstDayInRange = selectedDays.First();
            var lastDayInRange = selectedDays.Last();

            CreateMonthForRange(firstDayInRange, lastDayInRange);
            ApplySelectionForDays(firstDayInRange, lastDayInRange);
        }

        public void ClearSelection()
        {
            UnSelectAll();
            _daySelector.Reset();
        }

        public IEnumerable<CalendarCardDay> GetAllSingleSelectedDays()
        {
            return _calendar.Months.SelectMany(x => x.Days).Where(x => x.IsSelected).ToList();
        }

        public IEnumerable<CalendarCardDay> GetSelectedDaysForSelectionMode(CalendarSelectionMode selectionMode)
        {
            if (selectionMode == CalendarSelectionMode.Single)
                return GetAllSingleSelectedDays();

            var selectedDays = _calendar.Months.SelectMany(x => x.Days).Where(x => x.IsSelected).ToList();

            return new[] {selectedDays.FirstOrDefault(), selectedDays.LastOrDefault()};
        }

        public CalendarMonth GetOrCreateMonthForDay(DateTime currentDate)
        {
            var calendarMonth = _calendar.GetMonth(currentDate);

            if (calendarMonth != null)
            {
                return calendarMonth;
            }

            CreateMonthForDay(currentDate);

            return _calendar.GetMonth(currentDate);
        }

        public void HandleDaySelected(CalendarCardDay calendarCardDay)
        {
            _daySelector.HandleSelection(calendarCardDay);
        }

        private void CreateMonthForRange(DateTime firstDayInRange, DateTime lastDayInRange)
        {
            var firstDayMonth = firstDayInRange.FirstDayOfMonth();
            var lastDayMonth = lastDayInRange.FirstDayOfMonth();

            while (firstDayMonth <= lastDayMonth)
            {
                var currentMonth = _calendarDaysProvider.GenerateCalendarMonth(firstDayMonth, this);
                _calendar.AddMonthIfNotExist(currentMonth);
                firstDayMonth = firstDayMonth.AddMonths(1);
            }

            CreateMonthForDay(firstDayMonth);
            CreateMonthForDay(firstDayMonth);
        }

        private void CreateMonthForDay(DateTime currentDate)
        {
            var previousDate = currentDate.AddMonths(-1);
            var nextDate = currentDate.AddMonths(1);

            if (!_calendar.CheckIfMonthForDateExist(currentDate))
            {
                var currentMonth = _calendarDaysProvider.GenerateCalendarMonth(currentDate, this);
                _calendar.AddMonthIfNotExist(currentMonth);
            }

            if (!_calendar.CheckIfMonthForDateExist(previousDate))
            {
                var previousMonth = _calendarDaysProvider.GenerateCalendarMonth(previousDate, this);
                _calendar.AddMonthIfNotExist(previousMonth);
            }

            if (!_calendar.CheckIfMonthForDateExist(nextDate))
            {
                var nextMonth = _calendarDaysProvider.GenerateCalendarMonth(nextDate, this);
                _calendar.AddMonthIfNotExist(nextMonth);
            }
        }

        public void ApplySelectionForDays(DateTime startDate, DateTime endDate)
        {
            UnSelectAll();

            var monthsForRange = _calendar.GetMonthsWithDays(startDate, endDate).ToList();

            foreach (var month in monthsForRange)
            {
                foreach (var day in month.Days.Where(x =>
                             x.DateTime.Date >= startDate.Date && x.DateTime.Date <= endDate.Date))
                {
                    day.IsSelected = true;
                }
            }

            for (int i = 0; i < monthsForRange.Count(); i++)
            {
                ApplySelectionForDaysInMont(GetMonthForIndex(i - 1, monthsForRange),
                    GetMonthForIndex(i, monthsForRange), GetMonthForIndex(i + 1, monthsForRange));
            }
        }

        private CalendarMonth GetMonthForIndex(int index, IList<CalendarMonth> months)
        {
            if (index < 0)
                return null;

            if (index >= months.Count())
                return null;

            return months[index];
        }

        private void ApplySelectionForDaysInMont(CalendarMonth previousMonth, CalendarMonth currentMonth,
            CalendarMonth nextMonth)
        {
            var selectedDays = currentMonth.Days.Where(x => x.IsSelected).ToList();
            var daysIndexes = selectedDays.Count() - 1;

            for (int i = 0; i <= daysIndexes; i++)
            {
                var selectedDay = selectedDays[i];

                if (i == 0)
                {
                    if (previousMonth == null)
                    {
                        selectedDay.ApplySelection(SelectionType.FirstInRange);
                        continue;
                    }

                    var oneDayBeforeInPreviousMonth =
                        previousMonth.Days.FirstOrDefault(x => x.Equals(selectedDay.DateTime.AddDays(-1)));

                    if (oneDayBeforeInPreviousMonth != null && !oneDayBeforeInPreviousMonth.IsSelected)
                    {
                        selectedDay.ApplySelection(SelectionType.FirstInRange);
                        continue;
                    }

                    selectedDay.ApplySelection(SelectionType.InRange);
                }
                else if (i == daysIndexes)
                {
                    if (nextMonth == null)
                    {
                        selectedDay.ApplySelection(SelectionType.LastInRange);
                        continue;
                    }

                    var oneDayAfterInNextMonth =
                        nextMonth.Days.FirstOrDefault(x => x.Equals(selectedDay.DateTime.AddDays(1)));

                    if (oneDayAfterInNextMonth != null && !oneDayAfterInNextMonth.IsSelected)
                    {
                        selectedDay.ApplySelection(SelectionType.LastInRange);
                        continue;
                    }

                    selectedDays[i].ApplySelection(SelectionType.InRange);
                }
                else
                {
                    selectedDays[i].ApplySelection(SelectionType.InRange);
                }
            }
        }


        public void ApplySelectionForDay(DateTime dayForSelection, bool withCleanUp = false)
        {
            if (withCleanUp)
                UnSelectAll();

            var selectedDays = _calendar.GetDaysFromCalendarForDate(dayForSelection);

            foreach (var day in selectedDays)
            {
                day.ApplySelection(SelectionType.Single);
            }
        }

        public void ApplyUnSelectionForDay(DateTime dayForSelection)
        {
            var selectedDays = _calendar.GetDaysFromCalendarForDate(dayForSelection);

            foreach (var day in selectedDays)
            {
                day.ApplySelection(SelectionType.None);
            }
        }

        private void UnSelectAll()
        {
            var selectedDays = _calendar.Months.SelectMany(x => x.Days)
                .Where(x => x.SelectionType != SelectionType.None);

            foreach (var selectedDay in selectedDays)
            {
                selectedDay.ApplyUnSelection();
            }
        }
    }
}