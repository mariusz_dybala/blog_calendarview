using System;
using FreshMvvm;
using CalendarView.Interfaces;
using CalendarView.Models;

namespace CalendarView.Services
{
    public class EditStrategy : IAddOrEditStrategy
    {
        public string ButtonText => "Edit";
        public string HeaderText => "Edit your list";
        public ShoppingList ShoppingListModel { get; }

        public EditStrategy(ShoppingList shoppingList)
        {
            ShoppingListModel = shoppingList;
        }

        public void OnButtonClicked(ShoppingList shoppingList, IPageModelCoreMethods coreMethods)
        {
            if (coreMethods == null)
            {
                throw new NullReferenceException("Core methods need to be set. Execute Init() method first.");
            }
            
            var newShoppingList = new ShoppingList
            {
                Description = shoppingList.Description,
                IsOpened = true,
                Id = shoppingList.Id,
                Owner = shoppingList.Owner,
                Name = shoppingList.Name,
                Type = shoppingList.Type
            };

            coreMethods.PopPageModel(PageMessage.AsEdit(newShoppingList), true, true);
        }
    }
}