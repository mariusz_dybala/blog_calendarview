using FreshMvvm;
using CalendarView.Models;

namespace CalendarView.Interfaces
{
    public interface IAddOrEditStrategy
    {
        string ButtonText { get;}
        string HeaderText { get; }
        ShoppingList ShoppingListModel { get; }
        void OnButtonClicked(ShoppingList shoppingList, IPageModelCoreMethods coreMethods);
    }
}