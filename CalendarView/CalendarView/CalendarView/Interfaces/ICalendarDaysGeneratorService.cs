using System;
using CalendarView.Models;

namespace CalendarView.Interfaces
{
    public interface ICalendarDaysGeneratorService
    {
        CalendarMonth GenerateMonthForCalendarDay(DateTime selectedDay);
    }
}