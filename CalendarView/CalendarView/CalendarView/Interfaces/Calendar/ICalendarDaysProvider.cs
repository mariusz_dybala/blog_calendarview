using System;
using CalendarView.Models;

namespace CalendarView.Interfaces.Calendar
{
    public interface ICalendarDaysProvider
    {
        CalendarMonth GenerateCalendarMonth(DateTime dayOfMoth, Services.CalendarService calendarService);
    }
}