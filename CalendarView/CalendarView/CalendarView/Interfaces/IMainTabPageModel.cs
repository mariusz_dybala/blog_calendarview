using System;
using CalendarView.Models;

namespace CalendarView.Interfaces
{
    public interface IMainTabPageModel
    {
        void ItemClosed(ShoppingList shoppingList);
        event Action<ShoppingList> OnItemClosed;
        event Action<ShoppingList> OnItemAdded;
        event Action<ShoppingList> OnItemEdited;
    }
}