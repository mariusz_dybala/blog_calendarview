namespace CalendarView.Enums
{
    public enum CalendarSelectionMode
    {
        Single,
        Range
    }
}