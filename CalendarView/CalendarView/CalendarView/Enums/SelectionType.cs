namespace CalendarView.Enums
{
    public enum SelectionType
    {
        None,
        FirstInRange, 
        LastInRange, 
        InRange, 
        Single
    }
}