using CalendarView.Controls;
using CalendarView.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenedListsPage : TabPage
    {
        public override StackLayout SelectionIndicator => (StackLayout) GetTemplateChild("OpenedListsTabSelector");
        public OpenedListsPage()
        {
            InitializeComponent();
            
            TabIndex = ((TabButton) GetTemplateChild("OpenedListsTab")).TabButtonIndex;
        }
    }
}