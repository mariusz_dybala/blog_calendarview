using CalendarView.Controls;
using CalendarView.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ClosedListsPage : TabPage
    {
        public override StackLayout SelectionIndicator => (StackLayout) GetTemplateChild("ClosedListsTabSelector");

        public ClosedListsPage()
        {
            InitializeComponent();

            TabIndex = ((TabButton) GetTemplateChild("ClosedListsTab")).TabButtonIndex;
        }
    }
}