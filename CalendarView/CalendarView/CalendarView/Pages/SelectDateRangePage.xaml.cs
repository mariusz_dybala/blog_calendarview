using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SelectDateRangePage : ContentPage
    {
        public SelectDateRangePage()
        {
            InitializeComponent();
        }
    }
}