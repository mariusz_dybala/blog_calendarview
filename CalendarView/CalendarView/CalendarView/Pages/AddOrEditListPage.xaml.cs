using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CalendarView.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddOrEditListPage : ContentPage
    {
        public AddOrEditListPage()
        {
            InitializeComponent();
        }
    }
}